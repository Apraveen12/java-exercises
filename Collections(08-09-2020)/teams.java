package hcl_assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class teams {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner s=new Scanner(System.in);
		String name;
		int number;
		ArrayList<ipl> a=new ArrayList<>();
		System.out.println("Enter the number of teams");
		int n=s.nextInt();
		s.nextLine();
		for(int i=1;i<=n;i++) {
			System.out.println("Enter team "+i+" details");
			System.out.println("Enter Team name :");
			name=s.nextLine();
			System.out.println("Enter number of Matches");
			number=s.nextInt();
			a.add(new ipl(name,number));
			s.nextLine();
		}
		Collections.sort(a,new NumberComparator());
		System.out.println("Team list after sort by number of matches");
		a.forEach(System.out::println);
	}

}
