package hcl_assignments;

public class iplteams {
	String name;
	String team;
	String role;
	public iplteams(String name, String team, String role) {
		super();
		this.name = name;
		this.team = team;
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "--"+ name + "--" + team +"--" + role;
	}
	

}
