package com.pack.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pack.model.Login;

@Controller
public class LoginController {
	@RequestMapping(value="/loginRequest")
	public String login(ModelMap m) {
		Login l=new Login();
	    m.addAttribute("log", l);
		return "login";
	}
	@RequestMapping(value="/check")
	public String login1(@Validated @ModelAttribute("log")Login l,BindingResult result,ModelMap m) {
		String s="";
		if(result.hasErrors()) {
			s="login";
		}
		else {
		m.addAttribute("login", l);
	        s="loginsuccess";
		}
		return s;
	}
	

}
