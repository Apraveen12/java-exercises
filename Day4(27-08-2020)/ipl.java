import java.util.Scanner;

class TeamNameNotFoundException extends Exception {
	String message;
	public TeamNameNotFoundException(String message) {
		super(message);
		this.message=message;
	}
}
public class ipl {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String teams = "Chennai Super Kings,Deccan Chargers,Delhi Daredevils,Kings XI Punjab,Kolkata Knight Riders,Mumbai Indians,Rajasthan Royals,Royal Challengers Bangalore";
		Scanner s = new Scanner(System.in);
		int winnerFlag = 0;
		int runnerFlag = 0;
		String runner, winner;
		try {
			System.out.println("Enter the expected winner team of IPL Season 4");
			winner = s.nextLine();
			if (teams.contains(winner)) {
				winnerFlag = 1;
			} else {
				throw new TeamNameNotFoundException(
						"TeamNameNotFoundException: Entered team is not a part of IPL Season 4");
			}
			System.out.println("Enter the expected runner team of IPL Season 4");
			runner = s.nextLine();
			if (teams.contains(runner)) {
				runnerFlag = 1;
			} else {
				throw new TeamNameNotFoundException(
						"TeamNameNotFoundException: Entered team is not a part of IPL Season 4");
			}
			if (winnerFlag == 1 && runnerFlag == 1) {
				System.out.println("Expected IPL Season 4 winner:" + winner);
				System.out.println("Expected IPL Season 4 runner:" + runner);
			}
		} catch (TeamNameNotFoundException t) {
			System.out.println(t.getMessage());
		}
	}
}
