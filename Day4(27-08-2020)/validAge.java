import java.util.Scanner;

class InvalidAgeRangeException extends Exception {
	String message;

	public InvalidAgeRangeException(String message) {
		super(message);
		this.message = message;
	}

	
}

public class validAge {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the player name");

		Scanner s = new Scanner(System.in);
		String name = s.nextLine();
		int age;
		int flag = 1;
		try {
			System.out.println("enter age");
			age = s.nextInt();
			if (age < 39 && age > 19) {
				flag = 1;
			} else {
				flag=0;
				throw new InvalidAgeRangeException("CustomException: InvalidAgeRangeException");
			}
			if (flag == 1) {
				System.out.println("player name :"+name);
				System.out.println("player age :"+age);

		}
		} catch (InvalidAgeRangeException i) {
			System.out.println(i.getMessage());
		}
	}

}
