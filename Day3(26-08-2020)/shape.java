import java.util.Scanner;

class shapes{
protected String shapeName;

public shapes(String shapeName) {
	super();
	this.shapeName = shapeName;
}
public double calculateArea() {
	return 0.0;
}
}
class Square extends shapes{
	private int side;

	public Square(String shapeName, int side) {
		super(shapeName);
		this.side = side;
	}
	public double calculateArea() {
		return side*side;
	}
}
class Rectangle extends shapes{
	private int length;
	private int breadth;
	public Rectangle(String shapeName, int length, int breadth) {
		super(shapeName);
		this.length = length;
		this.breadth = breadth;
	}
	public double calculateArea() {
		return length*breadth;
	}
}
class Circle extends shapes{
	private int radius;

	public Circle(String shapeName, int radius) {
		super(shapeName);
		this.radius = radius;
	}
	public double calculateArea() {
		return 3.14*radius*radius;
	}
	
}
class shape{
	public static void main(String args[]) {
		Scanner s=new Scanner(System.in);
		System.out.println("1.Rectangle\n2.Square\n3.Circle");
		System.out.println("Area Calculator --- Choose your Shape");
		int choice=s.nextInt();
		s.hasNextLine();
		switch(choice) {
		case 1:
			System.out.println("Enter length and bredth");
			int l=s.nextInt();
			int b=s.nextInt();
			Rectangle r=new Rectangle("rectangle", l, b);
			System.out.println("Area of rectangle :"+r.calculateArea());
			break;
		case 2:
			System.out.println("Enter side");
			int side=s.nextInt();
			Square sq=new Square("square", side);
			System.out.println("Area of square :"+sq.calculateArea());
			break;
		case 3:
			System.out.println("Enter radius");
			int rad=s.nextInt();
			Circle c=new Circle("Circle", rad);
			System.out.println("Area of Circle :"+c.calculateArea());

		}
		
		
	}
}