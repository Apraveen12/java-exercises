import java.util.Scanner;
class Delivery{
long over,ball,runs;
String batsman,bowler,nonStriker;
Delivery(long over,long ball,long runs,String batsman,String bowler,String nonStriker){
this.over=over;
this.ball=ball;
this.runs=runs;
this.bowler=bowler;
this.batsman=batsman;
this.nonStriker=nonStriker;
}
void displayDeliveryDetails(){
System.out.println("Delivery Details:"); 
System.out.println("Over :"+over);
System.out.println("Ball :"+ball);
System.out.println("Runs :"+runs);
System.out.println("Batsman :"+batsman);
System.out.println("Bowler :"+bowler);
System.out.println("NonStriker :"+nonStriker);
}
}
class Main
{
public static void main(String args[]){
Scanner s=new Scanner(System.in);
System.out.println("Enter the over");
long over=Long.parseLong(s.nextLine());
System.out.println("Enter the ball");
long ball=Long.parseLong(s.nextLine());
System.out.println("Enter the runs");
long runs=Long.parseLong(s.nextLine());
System.out.println("Enter the batsman name");
String batsman=s.nextLine();
System.out.println("Enter the bowler name");
String bowler=s.nextLine();
System.out.println("Enter the nonStriker name");
String nonStriker=s.nextLine();
Delivery dv=new Delivery(over,ball,runs,batsman,bowler,nonStriker);
dv.displayDeliveryDetails();
}
}
