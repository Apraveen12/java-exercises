import java.util.Scanner;
 class Venue1 {
	 private String name;
	 private String city;
	public Venue1(String name, String city) {
		super();
		this.name = name;
		this.city = city;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	void display() {
		System.out.println("Venue Name: "+name);
		System.out.println("City Name: "+city);
	}
}
class Main{
	public static void main(String args[]) {
		Scanner s=new Scanner(System.in);
		System.out.println("enter the venue name");
		String name=s.nextLine();
		System.out.println("enter the city name");
		String city=s.nextLine();
		Venue1 v1=new Venue1(name,city);
		v1.display();
		System.out.println("verify and update details");
		System.out.println("Menu \n 1.Update Venue Name \n 2.Update City Name \n 3.All Informations Correct/Exit");
		
		int choice=s.nextInt();
		s.nextLine();
		switch(choice)
		{
		case 1:
						System.out.println("enter the venue name");
				String venue2=s.nextLine();
				v1.setName(venue2);
				v1.display();
				break;
		case 2:
			
				System.out.println("enter the city name");
				String city2=s.nextLine();
				v1.setCity(city2);
				v1.display();
				break;
		case 3:
				v1.display();
				break;
		}
		
	}
}