import java.util.Scanner;

class ExtraType{
	String name;
	long runs;
	
	public ExtraType(String name, long runs) {
		super();
		this.name = name;
		this.runs = runs;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getRuns() {
		return runs;
	}
	public void setRuns(long runs) {
		this.runs = runs;
	}
	public void display() {
		System.out.println("extratype details");
		System.out.println("extra type:"+name);
		System.out.println("Runs:"+runs);
	}
	
}
public class Main2{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the extratype details");
		String etype= s.nextLine();
		String[] etype1 =etype.split("#");
		String type=etype1[0];
		long runs=Long.parseLong(etype1[1]);
		ExtraType et=new ExtraType(type,runs);
		et.display();
		
	}

}
