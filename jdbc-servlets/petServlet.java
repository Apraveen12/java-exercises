package com.pets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class petServlet
 */
@WebServlet("/petServlet")
public class petServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public petServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		String fname=request.getParameter("fname");
		String lname=request.getParameter("lname");
		String address=request.getParameter("address");
		String email=request.getParameter("email");
		String city=request.getParameter("city");
		String state=request.getParameter("state");
		String pname=request.getParameter("pname");
		String ptype=request.getParameter("ptype");
		String phone=request.getParameter("phone");
		String age=request.getParameter("age");
		Connection con=null;
		try {
			 Class.forName("com.mysql.cj.jdbc.Driver");
             con=DriverManager.getConnection("jdbc:mysql://localhost:3306/pets", "root","root");
             PreparedStatement ps=con.prepareStatement("insert into petregistration(FirstName,LastName,Address,Email,City,State,PetName,PetType,PhoneNumber,PetAge)values(?,?,?,?,?,?,?,?,?,?)");
             ps.setString(1, fname);
             ps.setString(2, lname);
             ps.setString(3, address);
             ps.setString(4, email);
             ps.setString(5, city);
             ps.setString(6, state);
             ps.setString(7, pname);
             ps.setString(8, ptype);
             ps.setString(9, phone);
             ps.setString(10, age);
             int i=ps.executeUpdate();
             if(i==1) {
            	 System.out.println("registerd sucessfully");
            	 }
		
		
		}catch(Exception e) {
			System.out.println(e);
			
		}
		
		
	}

}
